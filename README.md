# Pachingo

## The Galton Board
A Galton board, bean machine, or quincunx, invented by Sir Francis Galton, is a Panchinko like machine with balls that fall, bouncing down a triangular arrangement of pegs into one of several columns.
The Galton board demonstrates randomness, chaos, and the central limit theorem.

## The Model
Pachingo models a Galton board using Go channels (the slots between pegs), Goroutines (the pegs which direct the ball one way or the other), and empty structs (the balls).
These "balls" accumulate in channels (modelling the columns) with sufficient capacity to hold several "balls".
The chaotic behavior of a peg is modeled with Go's select statement, which randomizes the two cases in some fashion dependent on the implementation of the compiler.
This is most likely a chaotic, pseudorandom randomization, rather than a truly random randomization.

## The Result
A single ball's trajectory is highly chaotic, but on average the distribution of balls is binomial, showing the central limit theorem.

```
$ go run main.go



o
oo
oooo
ooooooooooooo
ooooooooooooooooooo
oooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooo
ooooooooooooooooooooooo
oooooooooooooooooooooooooo
ooooooooo
ooooo
oo





```
