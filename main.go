package main

import (
	"fmt"
)

func peg(in <-chan struct{}, out1 chan<- struct{}, out2 chan<- struct{}) {
	for ball := range in {
		select {
		case out1 <- ball:
		case out2 <- ball:
		}
	}
	close(out1)
	close(out2)
}

func pegs(slots []chan struct{}) chan<- struct{} {
	if len(slots) == 1 {
		return slots[0]
	}
	slots_ := make([]chan struct{}, len(slots) - 1)
	for n := range slots_ {
		slots_[n] = make(chan struct{})
		go peg(slots_[n], slots[n], slots[n + 1])
	}
	return pegs(slots_)
}

func main() {
	bins := 20
	count := 200
	columns := make([]chan struct{}, bins)
	for n := range columns {
		columns[n] = make(chan struct{}, count)
	}
	in := pegs(columns)
	for n := 0; n < count; n++ {
		in <- struct{}{}
	}
	for _, col := range columns {
		for n := 0; n < len(col); n++ {
			fmt.Print("o")
		}
		fmt.Println()
	}
	close(in)
}
